describe('Desktop Header Feature', () => {
  beforeEach(() => {
    cy.visitDesktop('/');
  });

  it('has grinfer logo', () => {
    cy.get('a[href="/"]')
      .should('be.visible');
  });

  it('open sign-in page by pressing login button', () => {
    cy.get('a[href="/sign-in"]')
      .click()
      .url()
      .should('eq', `${Cypress.config().baseUrl}sign-in`);
  });

  it('open sign-up page by pressing Create Account button', () => {
    cy.get('a[href="/sign-up"]')
      .click()
      .url()
      .should('eq', `${Cypress.config().baseUrl}sign-up`);
  });

});