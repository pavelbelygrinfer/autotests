describe('Mobile Toolbar Feature', () => {
  beforeEach(() => {
    cy.visitMobile('/')
  });

  it('has toolbar buttons', () => {
    cy.contains('Discover').should('exist');
    cy.contains('Search').should('exist');
    cy.contains('My courses').should('exist');
    cy.contains('Categories').should('exist');
    cy.contains('Profile').should('exist');
  });

});