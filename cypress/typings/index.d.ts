declare namespace Cypress {
  interface Chainable {
    // Custom commands support/commands.ts
    visitDesktop(url: string): Chainable<Element>;
    visitTablet(url: string): Chainable<Element>;
    visitMobile(url: string): Chainable<Element>;
  }
}